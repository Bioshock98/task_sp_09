package ru.pyshinskiy.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.TaskDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TaskRestClient extends AbstractRestClient {

    @Nullable
    public TaskDTO getTask(@NotNull final String taskId) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", taskId);
        @Nullable final TaskDTO resultTask = restTemplate.getForObject(taskGetUrl, TaskDTO.class, params);
        return resultTask;
    }

    @NotNull
    public List<TaskDTO> getAllTasks() {
        @NotNull final ResponseEntity<List<TaskDTO>> response = restTemplate.exchange(
                taskAllUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TaskDTO>>(){});
        @NotNull final List<TaskDTO> tasks = response.getBody();
        return tasks;
    }

    public void saveTask(@NotNull final TaskDTO taskDTO) {
        @NotNull final TaskDTO savedTask = restTemplate.postForObject(taskSaveUrl, taskDTO, TaskDTO.class);
        System.out.println(savedTask);
    }

    public void updateTask(@NotNull final TaskDTO taskDTO) {
        restTemplate.put(taskUpdateUrl, taskDTO);
    }

    public void deleteTask(@NotNull final String id) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        restTemplate.delete(taskDeleteUrl, params);
    }
}
