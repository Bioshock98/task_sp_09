package ru.pyshinskiy.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import ru.pyshinskiy.tm.interceptor.StatefulRestTemplateInterceptor;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = "ru.pyshinskiy.tm")
public class ApplicationConfig {

    @Bean
    public RestTemplate restTemplate() {
        @NotNull final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new StatefulRestTemplateInterceptor());
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
