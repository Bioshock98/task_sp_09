package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.pyshinskiy.tm.api.AuthRestClient;
import ru.pyshinskiy.tm.api.ProjectRestClient;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.dto.ProjectDTO;

import java.util.Date;
import java.util.Random;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Category(ru.pyshinskiy.tm.IntegrateRestClientTest.class)
public class ProjectRestClientTest {

    @Autowired
    private ProjectRestClient projectRestClient;

    @Autowired
    private AuthRestClient authRestClient;

    @Before
    public void setUp() {
        authRestClient.sign_up("test", "1234");
        authRestClient.login("test", "1234");
    }

    @After
    public void tearDown() {
        authRestClient.acc_delete();
    }

    @Test
    public void getAllProjects() {
        for(int i = 0; i < 5; i++) {
            projectRestClient.saveProject(createProjectDTO());
        }
        Assert.assertEquals(5, projectRestClient.getAllProjects().size());
    }

    @Test
    public void updateProject() {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectRestClient.saveProject(projectDTO);
        projectDTO.setName("UPDATED");
        projectRestClient.updateProject(projectDTO);
        Assert.assertEquals(projectDTO.getName(), projectRestClient.getProject(projectDTO.getId()).getName());
    }

    @Test
    public void deleteProject() {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectRestClient.saveProject(projectDTO);
        projectRestClient.deleteProject(projectDTO.getId());
        Assert.assertNull(projectRestClient.getProject(projectDTO.getId()));
    }

    private ProjectDTO createProjectDTO() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(null);
        projectDTO.setName(new Random().toString());
        projectDTO.setDescription("test project description");
        projectDTO.setStartDate(new Date());
        projectDTO.setFinishDate(new Date());
        return projectDTO;
    }
}
