package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Task findTaskById(@NotNull final String id);

    @NotNull
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findTasksByProject(@NotNull final Project project);

    @NotNull
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Task> findTasksByUserId(@NotNull final String userId);

    @Nullable
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Task findTaskByIdAndUserId(@NotNull final String id, @NotNull final String userId);

    void deleteTaskByIdAndUserId(@NotNull final String taskId, @NotNull final String userId);
}
