package ru.pyshinskiy.tm.api.web;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.RoleType;
import ru.pyshinskiy.tm.service.user.IUserService;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Component
public class AccountController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserDTO user;

    public String register() {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        userService.save(dtoConverter.toUser(user));
        return "projects.xhtml?faces-redirect=true";
    }

    @PostConstruct
    public void initNewUser() {
        @NotNull final UserDTO userDTO = new UserDTO();
        @NotNull final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.ROLE_USER);
        userDTO.setRoles(roles);
        user = userDTO;
    }
}
