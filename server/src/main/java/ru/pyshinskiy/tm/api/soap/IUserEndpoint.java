package ru.pyshinskiy.tm.api.soap;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    List<UserDTO> getAllUsers();

    @WebMethod
    UserDTO getUser(@NotNull final String userId);

    @WebMethod
    UserDTO saveUser(@NotNull final UserDTO userDTO);

    @WebMethod
    void deleteUser(@NotNull final String userId);
}
