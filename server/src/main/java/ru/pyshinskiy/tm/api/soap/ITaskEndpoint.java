package ru.pyshinskiy.tm.api.soap;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    List<TaskDTO> getAllTasks();

    @WebMethod
    TaskDTO getTask(@NotNull final String taskId);

    @WebMethod
    TaskDTO saveTask(@NotNull final TaskDTO taskDTO);

    @WebMethod
    void deleteTask(@NotNull final String taskId);
}
