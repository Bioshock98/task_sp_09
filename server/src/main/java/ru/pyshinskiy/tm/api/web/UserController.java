package ru.pyshinskiy.tm.api.web;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.RoleType;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    private UserDTO selectedUser;

    @Nullable
    private UserDTO editingUser;

    @Nullable
    private UserDTO addingUser;

    @Nullable
    private UserDTO removingUser;

    @Nullable
    private List<UserDTO> filteredUsers;

    @NotNull
    private List<String> roles = Arrays.stream(RoleType.values()).map(Enum::toString).collect(Collectors.toList());

    @NotNull
    private String userAction = "User add";

    @NotNull
    public UserDTO getAddingUser() {
        @NotNull final UserDTO userDTO = new UserDTO();
        return userDTO;
    }

    public List<UserDTO> getUsers() {
        return userService.findAllUsersWithRoles()
                .stream()
                .map(e -> dtoConverter.toUserDTO(e))
                .collect(Collectors.toList());
    }

    public String updateUser() {
        editingUser.setPasswordHash(passwordEncoder.encode(editingUser.getPasswordHash()));
        userService.save(dtoConverter.toUser(editingUser));
        return "users.xhtml?faces-redirect=true";
    }

    public String deleteUser() {
        userService.remove(removingUser.getId());
        return "users.xhtml?faces-redirect=true";
    }
}
