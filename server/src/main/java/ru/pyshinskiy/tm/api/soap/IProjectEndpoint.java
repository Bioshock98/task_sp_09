package ru.pyshinskiy.tm.api.soap;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    List<ProjectDTO> getAllProjects();

    @WebMethod
    ProjectDTO getProject(@NotNull final String projectId);

    @WebMethod
    ProjectDTO saveProject(@NotNull final ProjectDTO projectDTO);

    @WebMethod
    void deleteProject(@NotNull final String projectId);
}
