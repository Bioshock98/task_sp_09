package ru.pyshinskiy.tm.api.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestAuthResult {

    private String status;
}
