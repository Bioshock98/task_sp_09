package ru.pyshinskiy.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Cacheable;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Cacheable
@MappedSuperclass
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AbstractEntity implements Serializable {

    private static final long SerialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();
}
