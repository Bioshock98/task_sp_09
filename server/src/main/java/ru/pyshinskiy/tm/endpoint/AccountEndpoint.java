package ru.pyshinskiy.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.soap.IAccountEndpoint;

import javax.jws.WebService;

@Getter
@Setter
@Component
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.soap.IAccountEndpoint")
public class AccountEndpoint implements IAccountEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public String login(@NotNull String username, @NotNull String password) {

        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    username,
                    password
            );
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return "200 OK";
        }
        catch (Exception e) {
            return "501 ACCESS DENIED";
        }
    }
}
