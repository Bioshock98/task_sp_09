package ru.pyshinskiy.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.soap.ITaskEndpoint;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.task.ITaskService;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.soap.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private DTOConverter dtoConverter;

    @Override
    public List<TaskDTO> getAllTasks() {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        return userPrincipal != null ? taskService.findAllTasksByUserId(userPrincipal.getId())
                .stream()
                .map(e -> dtoConverter.toTaskDTO(e))
                .collect(Collectors.toList()) : null;
    }

    @Override
    public TaskDTO getTask(@NotNull String taskId) {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        return userPrincipal != null ? dtoConverter.toTaskDTO(taskService.findTaskByIdAndUserId(taskId, userPrincipal.getId())) : null;
    }

    @Override
    public TaskDTO saveTask(@NotNull TaskDTO taskDTO) {
        taskService.save(dtoConverter.toTask(taskDTO));
        return taskDTO;
    }

    @Override
    public void deleteTask(@NotNull String taskId) {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        if(userPrincipal != null) taskService.removeByIdAndUserId(taskId, userPrincipal.getId());
    }

    private UserPrincipal getUserPrincipal() {
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPrincipal;
    }
}
