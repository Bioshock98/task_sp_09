package ru.pyshinskiy.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.pyshinskiy.tm.api.soap.IAccountEndpoint;
import ru.pyshinskiy.tm.api.soap.IProjectEndpoint;
import ru.pyshinskiy.tm.api.soap.ITaskEndpoint;
import ru.pyshinskiy.tm.api.soap.IUserEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class WebServiceConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private IAccountEndpoint accountEndpoint;

    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Autowired
    private IUserEndpoint userEndpoint;

    @Bean
    public Endpoint accountServiceEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, accountEndpoint);
        endpoint.publish("/accountEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectServiceEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/projectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskServiceEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/taskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint userServiceEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, userEndpoint);
        endpoint.publish("/userEndpoint");
        return endpoint;
    }
}
